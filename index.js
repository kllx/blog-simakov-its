const express = require('express')
const bodyParser = require('body-parser')
const config = require('./config')
const getDirectories = require('./func').getDirectories

const app = express()
// создаем парсер для данных application/x-www-form-urlencoded
const urlencodedParser = bodyParser.urlencoded({ extended: true })

app.use('/public', express.static('public'))
app.set('view engine', 'ejs')

app.get('/', (_req, res) => {
  getDirectories(config.data, (_err, content) => {
    res.render('index', { 'titleName': content })
  })
})

require('./routes/article')(app, getDirectories)
require('./routes/add')(app, getDirectories)
require('./routes/del')(app, getDirectories, urlencodedParser)
require('./routes/edit')(app, getDirectories)
require('./routes/404')(app)

app.listen(config.port)
