const fs = require('fs')

function getDirectories (path, callback) { // функция обертка над readdir
  fs.readdir(path, (err, content) => {
    if (err) return callback(err)
    callback(null, content)
  })
}

module.exports = {
  getDirectories: getDirectories
}
