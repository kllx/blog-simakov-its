const multer = require('multer')
const path = require('path')

// Определения места хранения переданных изображений
const storage = multer.diskStorage({
  destination: './public/images/',
  filename: function (_req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
  }
})

// Проверка что фаил является изображением
function checkFileType (file, cb) {
  const filetypes = /jpeg|jpg|png|gif/ // регулярное выражение с расширениями файла изображения
  const extname = filetypes.test(path.extname(file.originalname).toLowerCase()) // проверка имени файла
  const mimetype = filetypes.test(file.mimetype) // проверка типа файла
  const err = 'Ошибка доступна загрузка только для изображений'

  if (mimetype && extname) {
    return cb(null, true)
  } else {
    return cb(err)
  }
}
// Настройка загрузки полученных файлов
const upload = multer(
  {
    storage: storage,
    limits: { fileSize: 5000000 },
    fileFilter: function (req, file, cb) {
      checkFileType(file, cb)
    }

  }
).single('img')

module.exports = {
  upload: upload
}
