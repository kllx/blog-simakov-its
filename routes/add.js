const fs = require('fs')
const config = require('./../config')
const upload = require('./../upload').upload

/*
multipart/form-data
post /add
KEY : VALUE
  title: имя статьи
  titleChapter1: Заголовок первой главы
  textChapter1: Текст первой главы
  titleChapter2: Заголовок второй главы
  textChapter2: Текст второй главы
  password: 123
*/

// ******** Страница добавления статьи **********************************************************
module.exports = async (app, getDirectories) => {
  app.get('/add', (_req, res) => { // добавление статьи
    res.render('add', { msg: '' })
  })
  app.post('/add', (req, res) => { // функция создания статьи
    upload(req, res, (err) => {
      if (err) { // проверка на наличие ошибок при загрузке
        res.render('add', {
          msg: err
        })
      } else {
        if (!req.body) return res.sendStatus(400) // проверка на наличие тела запроса
        if (req.body['g-recaptcha-response'] !== '' && req.body.password === config.password) { // проверка капчи и пароля
          getDirectories(config.data, (_arr, content) => {
            if (content.indexOf(req.body.title + '.json') === -1) { // проверка на уникальность названия статьи
              const filename = typeof req.file === 'undefined' ? '' : req.file.filename
              const articleData = JSON.stringify( // соединение глав в единый объект статьи и перевод в JSON формат

                { articleTitle: req.body.title, // заголовок статьи
                  FirstChapter: { // запись данных первой главы
                    titleChapter1: req.body.titleChapter1, // заголовок первой главы
                    textChapter1: req.body.textChapter1 // текст первой главы
                  },
                  SecondChapter: { // запись данных второй главы
                    titleChapter2: req.body.titleChapter2, // заголовок второй главы
                    textChapter2: req.body.textChapter2 // текст второй главы
                  },
                  ImgName: filename // сохранение названия картинки статьи
                })

              fs.writeFileSync(config.data + '/' + req.body.title + '.json', articleData) // создание нового файла с данными статьей
              res.redirect('/') // редирект на главную
            } else {
              res.render('add', {
                msg: 'Статья с таким именем уже существует'
              })
            }
          })
        }
      }
    })
  })
}
