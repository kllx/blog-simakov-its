module.exports = async (app) => {
  app.use('/', (_req, res) => {
    res.status(404)
    res.send('404')
  })
}
