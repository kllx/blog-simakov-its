const fs = require('fs')
const config = require('./../config')

/*
application/x-www-form-urlencoded
post /del
KEY : VALUE
  del: имя удаляемой статьи
  password: 123
*/

// ******** Страница удаления статьи *************************************************************
module.exports = async (app, getDirectories, urlencodedParser) => {
  app.get('/del', (_req, res) => {
    getDirectories(config.data, (_err, content) => {
      res.render('del', { 'titleName': content, 'msg': '' })
    })
  })

  app.post('/del', urlencodedParser, (req, res) => {
    if (!req.body) return res.sendStatus(400)
    else if (req.body['g-recaptcha-response'] !== '' && req.body.password === config.password) { // проверка капчи и пароля
      getDirectories(config.data, (_arr, content) => {
        if (content.indexOf(req.body.del + '.json') !== -1) { // проверка на существование файла переданной статьи
          fs.readFile(`./data/${req.body.del}`, (err, data) => { // получение данных статьи и рендер страницы с ними
            if (err) throw err
            const articleObj = JSON.parse(data) // получение данных статьи и перевод их в формат объекта
            const imgName = articleObj.ImgName // получение названия изображения из статьи
            if (imgName !== '' && imgName !== undefined) {
              fs.unlink(`${config.public}/images/${imgName}`, (err) => { // удаление старого файла изображения
                if (err) throw err
              })
            }
          })
          fs.unlink(`data/${(req.body.del)}.json`, () => {}) // удаление файла статьи
          res.redirect('/') // редирект на главную
        } else {
          res.render('del', {
            msg: 'Введено неверное название'
          })
        }
      })
    } else {
      res.render('del', {
        msg: 'Введено неверное название'
      })
    }
  })
}
