const fs = require('fs')
const config = require('./../config')
const upload = require('./../upload').upload

/*
multipart/form-data
post /add
KEY : VALUE
  title: новое имя статьи
  titleChapter1: новый заголовок первой главы
  textChapter1: новый текст первой главы
  titleChapter2: новый заголовок второй главы
  textChapter2: новый текст второй главы
  password: 123
*/

// ******** Страница изменения статьи **********************************************************
module.exports = async (app, getDirectories) => {
  app.get('/edit', (req, res) => {
    global.fileName = ''
    global.imgName = ''
    const linkArr = (req.headers.referer).split('/') // получение ulr ссылающейся страницы и преобразование ее в массив
    global.fileName = linkArr[linkArr.length - 1].indexOf('.json') !== -1 ? decodeURIComponent(linkArr[linkArr.length - 1]) : '' // получение имени файла
    if (global.fileName !== '') {
      fs.readFile(`./data/${global.fileName}`, (err, data) => { // получение данных статьи и рендер страницы с ними
        if (err) throw err
        const articleObj = JSON.parse(data) // преобразование в объект
        console.log(articleObj)
        global.imgName = articleObj.ImgName // получение названия изображения статьи
        res.render('edit', { data: articleObj, msg: '' })
      })
    } else {
      res.redirect('/')
    }
  })

  app.post('/edit', (req, res) => {
    upload(req, res, (err) => {
      if (err) { // проверка на наличие ошибок при загрузке
        res.render('add', {
          msg: err
        })
      } else {
        if (req.body['g-recaptcha-response'] !== '' && req.body.password === config.password) { // проверка капчи и пароля
          let imgname = typeof req.file === 'undefined' ? '' : req.file.filename // проверка на то что какой либо фаил был передан
          const articleData = JSON.stringify( // соединение глав в единый объект статьи и перевод в JSON формат
            { articleTitle: req.body.title, // заголовок статьи
              FirstChapter: { // запись данных первой главы
                titleChapter1: req.body.titleChapter1,
                textChapter1: req.body.textChapter1
              },
              SecondChapter: { // запись данных второй главы
                titleChapter2: req.body.titleChapter2,
                textChapter2: req.body.textChapter2
              },
              ImgName: imgname // сохранение названия картинки статьи
            })
          getDirectories(config.data, (_arr, content) => {
            if (content.indexOf(req.body.title + '.json') !== -1) {
              console.log(JSON.parse(articleData))
              res.render('edit', {
                msg: 'Статья с данным именем уже существует, выберите другое название',
                data: JSON.parse(articleData)
              })
            } else {
              fs.unlink(`${config.data}/${global.fileName}`, (err) => { // удаление старого файла статьи
                if (err) throw err
              })
              if (global.imgName !== '' && global.imgName !== undefined) { // удаление старого файла изображения
                fs.unlink(`${config.public}/images/${global.imgName}`, (err) => {
                  if (err) throw err
                })
              }
              fs.writeFileSync(config.data + '/' + req.body.title + '.json', articleData) // создание нового файла с данными статьей
              res.redirect('/') // редирект на главную
            }
          })
        }
      }
    })
  })
}
