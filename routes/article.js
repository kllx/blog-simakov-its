const fs = require('fs')
const config = require('./../config')

module.exports = async (app, getDirectories) => {
  app.get('/articles/:title', (req, res) => {
    getDirectories(config.data, (_arr, content) => { // смотрим содержимое data
      if (content.indexOf(req.params.title) !== -1) { // проверяем существует ли фаил со статьей
        fs.readFile(config.data + '/' + req.params.title, 'utf8', (_err, text) => { // считываем фаил со статьей
          const obj = JSON.parse(text)
          res.render('article', { // рендерим статью
            Title: req.params.title,
            ArticleObj: obj
          })
        })
      } else {
        res.render('404', {
          msg: 'Такой статьи не существует'
        })
      }
    })
  })
}
